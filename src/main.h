#ifndef MAIN__H_
#define MAIN__H_

//---------------------------------------------------

#define ASM_MAX_INSTRUCTION_COUNT 128

//#define ASM_MAX_LOCAL_SCALARS_COUNT 16
//#define ASM_MAX_LOCAL_VECTORS_COUNT 4
//
//#define ASM_MAX_CONDITIONAL_DEPTH_BITS 6
//#define ASM_MAX_CONDITIONAL_DEPTH (1 << (ASM_MAX_CONDITIONAL_DEPTH_BITS - 1))

#define ASM_INSTRUCTION_SIZE 8

//---------------------------------------------------

#define MAX_SCENE_RESOLUTION 1000000

//#define RAY_DATA_AND_OUTPUT_COLOR_PRELOAD_SIZE 2000

#define MAX_SCENE_OBJECTS 5

#define MAX_SCENE_LIGHTS 6

#define RAY_HIT_EPS 0.0001

#define HUGE_REAL_VAL 32767.0

#define MAX_EXTERNAL_REGISTER_NUM 4

//---------------------------------------------------

//#define USE_FIXEDPOINT

#ifdef USE_FIXEDPOINT

#include "ap_fixed.h"
#include "hls_math.h"
typedef ap_fixed<32, 16, AP_RND> myType;
typedef ap_fixed<24, 12, AP_RND>  myTypeReduced;

typedef ap_fixed<16, 8, AP_RND> pixelColorType;

//typedef ap_ufixed<8, 8, AP_RND, AP_SAT> pixelColorType;

#else

#include <cmath>
typedef float myType;

typedef short int pixelColorType;

//typedef unsigned char pixelColorType;

#endif


enum InstructionType{
	MOV, MOVV,
	ADD, ADDV,
	MUL, MULV, CMULV, DOT,
	MADD, MADDV, CMADDV,

//	MMULV, MMUL3V,
	MMULV,
	MMULDIR,
	TMMULDIR,

	PREDIV, PRESQRT,

	DELTA,
//	DIV,

//	POW,
//	DELTA,
//	SQRT,

//	MAG,
//	NORM,
//	REFL,

	IF_EQ, IF_GE, IF_GEQ, ELSE, ENDIF,	// conditionals

	HIT_SUCCESS, /*HIT_UNOBSCURED,*/ PREPARE_SHADING,

	IT_NONE = 1 << 7
};

// Instruction List parameter types; local - scalar or vector register
enum ParamType{
	LOCAL, SCALAR, VECTOR3,

	PT_NONE = 1 << 7
};

// Local register names
enum LocalVars {
//-- SCALARS
//	LV_A=1,
//	LV_B,
//	LV_C,
//	LV_D,
//	LV_E,
//	LV_F,
//	LV_G,
//	LV_H,
//	LV_I,
//	LV_J,
//	LV_K,
//	LV_L,
//	LV_M,
//	LV_N,
//	LV_O,
//	LV_P, // scalar registers

//	MATERIAL_AMBIENT_K, MATERIAL_AMBIENT_EXP,
//	MATERIAL_DIFFUSE_K, MATERIAL_DIFFUSE_EXP,
//	MATERIAL_SPECULAR_K, MATERIAL_SPECULAR_EXP,
//
//	LIGHT0_RADIANCE,

//-- VECTORS' SCALAR ELEMENTS
//	ZERO = 0, EPS, DIST,
//	BESTHIT_ISHIT = 4,	BESTHIT_DIST,	BESTHIT_OBJIDX,
//	ONE = 8, TWO, THREE,
//	LV_V3a_0 = 12, LV_V3a_1, LV_V3a_2, // *_0 element must divide by 4
//	LV_V3b_0 = 16, LV_V3b_1, LV_V3b_2,
//	LV_V3c_0 = 20, LV_V3c_1, LV_V3c_2,
//	LV_V3d_0 = 24, LV_V3d_1, LV_V3d_2,
//
//	RAY_ORIGIN_0 = 28, RAY_ORIGIN_1, RAY_ORIGIN_2,
//	RAY_ORIGIN_TRANSFORMED_0 = 32, RAY_ORIGIN_TRANSFORMED_1, RAY_ORIGIN_TRANSFORMED_2,
//	RAY_DIRECTION_0 = 36, RAY_DIRECTION_1, RAY_DIRECTION_2,
//	RAY_DIRECTION_TRANSFORMED_0 = 40, RAY_DIRECTION_TRANSFORMED_1, RAY_DIRECTION_TRANSFORMED_2,

//-- VECTORS
	LV_NONE = 1, // MAY BE USED AS TEMPvEC -> other parts of code can not rely on its contents !!!
	LV_V3_ZERO_EPS_DIST,
	LV_V3_ONE_TWO_THREE,
	LV_V3_HALF_QUATER_THREEHALFS,
	BESTHIT_ISHIT_DIST_OBJIDX,

	LV_V3a,
	LV_V3b,
	LV_V3c,
	LV_V3d,	//vector registers
	LV_V3e,
	LV_V3f,
	LV_V3g,
	LV_V3h,

	LV_VEXT0, LV_VEXT1, LV_VEXT2, LV_VEXT3,		// special registers for loaded data

	RAY_ORIGIN, RAY_ORIGIN_TRANSFORMED,
	RAY_DIRECTION, RAY_DIRECTION_TRANSFORMED,

	LV_V3_NORMAL_100, LV_V3_NORMAL_010, LV_V3_NORMAL_001, // ? vec(zero) -> x,y,z = 1

	SR_NORMAL,
	SR_LOCALHITPOINT, //special purpose vectors

	BESTHIT_NORMAL,
	BESTHIT_LOCALHITPOINT,
	BESTHIT_HITPOINT,
	BESTHIT_COLOR,

//	MATERIAL_AMBIENT_COLOR, MATERIAL_DIFFUSE_COLOR, MATERIAL_SPECULAR_COLOR,
//
//	LIGHT_RADIANCE012 = 160, LIGHT_RADIANCE345,
//	LIGHT0_COLOR = 162, 		//step results from MAX_SCENE_LIGHTS
//	LIGHT0_POSITION = 168,
//	LIGHT0_K = 174,


//-- MATRICES
	TRANS_MAT_0 = 64, TRANS_MAT_0_INV // object transformation matrices
};

struct vec3
{
//	vec3()
//	{
//		data[0] = data[1] = data[2] = 0.0;
//	}

	myType data[3];
	vec3()
	{
#pragma HLS ARRAY_PARTITION variable=data complete dim=1

	}
#ifndef USE_FIXEDPOINT
	vec3(myType x, myType y, myType z)
	{
		data[0] = x;
		data[1] = y;
		data[2] = z;
	}
#endif
	vec3 operator=(const vec3& v)
	{
		this->data[0] = v.data[0];
		this->data[1] = v.data[1];
		this->data[2] = v.data[2];
		return *this;
	}

	vec3 operator+(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = this->data[0] + v.data[0];
		temp.data[1] = this->data[1] + v.data[1];
		temp.data[2] = this->data[2] + v.data[2];
		return temp;
	}

	vec3 operator-(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = this->data[0] - v.data[0];
		temp.data[1] = this->data[1] - v.data[1];
		temp.data[2] = this->data[2] - v.data[2];
		return temp;
	}

	vec3 operator-() const
	{
		vec3 temp;
		temp.data[0] = -this->data[0];
		temp.data[1] = -this->data[1];
		temp.data[2] = -this->data[2];
		return temp;
//		return vec3(-this->data[0], -this->data[1], -this->data[2]);
	}


#ifdef USE_FIXEDPOINT
	vec3 operator*(myTypeReduced s) const
	{
		vec3 temp;
		temp.data[0] = (myTypeReduced)this->data[0] * s;
		temp.data[1] = (myTypeReduced)this->data[1] * s;
		temp.data[2] = (myTypeReduced)this->data[2] * s;
		return temp;
	}
#else
	vec3 operator*(myType s) const
	{
		vec3 temp;
		temp.data[0] = this->data[0] * s;
		temp.data[1] = this->data[1] * s;
		temp.data[2] = this->data[2] * s;
		return temp;
	}
#endif

#ifdef USE_FIXEDPOINT
	vec3 CompWiseMul(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = (myTypeReduced)this->data[0] * (myTypeReduced)v.data[0];
		temp.data[1] = (myTypeReduced)this->data[1] * (myTypeReduced)v.data[1];
		temp.data[2] = (myTypeReduced)this->data[2] * (myTypeReduced)v.data[2];
		return temp;
	}
#else
	vec3 CompWiseMul(const vec3& v)
	{
		vec3 temp;
		temp.data[0] = this->data[0] * v.data[0];
		temp.data[1] = this->data[1] * v.data[1];
		temp.data[2] = this->data[2] * v.data[2];
		return temp;
	}
#endif

#ifdef USE_FIXEDPOINT
	myType operator*(const vec3& v) const
	{
		return (myTypeReduced)this->data[0] * (myTypeReduced)v.data[0] +
			   (myTypeReduced)this->data[1] * (myTypeReduced)v.data[1] +
			   (myTypeReduced)this->data[2] * (myTypeReduced)v.data[2];
	}
#else
	myType operator*(const vec3& v) const
	{
		return this->data[0] * v.data[0] + this->data[1] * v.data[1] + this->data[2] * v.data[2];
	}
#endif

	vec3 operator/(myType s) const
	{
#ifdef USE_FIXEDPOINT
		return (*this) * (myTypeReduced)((myTypeReduced)1.0/((myTypeReduced)s));
#else
		return (*this) * ((myType)1.0/s);
#endif
//		return (*this) * t;
	}

	vec3 operator^(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = this->data[1] * v.data[2] - this->data[2] * v.data[1];
		temp.data[1] = this->data[2] * v.data[0] - this->data[0] * v.data[2];
		temp.data[2] = this->data[0] * v.data[1] - this->data[1] * v.data[0];
		return temp;
	}

	myType Magnitude() const
	{
#ifdef USE_FIXEDPOINT
		return hls::sqrt((*this) * (*this));
#else
		return std::sqrt((*this) * (*this));
#endif
	}

	vec3 Normalize() const
	{
#ifdef USE_FIXEDPOINT
		return (*this) / hls::sqrt((*this) * (*this));
#else
		return (*this) / std::sqrt((*this) * (*this));
#endif
	}

	void DecodeFromStream(const myType* stream)
	{
		for (unsigned i = 0; i < 3; ++i)
			data[i] = *(stream + i);
	}

	vec3 Reflect(const vec3& normal) const
	{
		return (normal * (normal * (*this)) * (myType)2.0) - (*this);
	}
};

struct mat4
{
	myType data[12]; // is 16 really needed? Nope...

	mat4()
	{
#pragma HLS ARRAY_PARTITION variable=data complete dim=1
	}

	mat4 operator=(const mat4& mat)
	{
		for (unsigned i = 0; i < 12; ++i) data[i] = mat.data[i];
		return *this;
	}

	mat4 operator*(const myType m) const
	{
		mat4 temp;
		for (unsigned i = 0; i < 12; ++i)
		{
			temp.data[i] = data[i] * m;
		}
		return temp;
	}

	mat4 operator*(const mat4& m) const
	{
		mat4 temp;
		myType s;// = (myType)0.0;

		for (unsigned i = 0; i < 3; ++i)
		{
			for (unsigned j = 0; j < 4; ++j)
			{
				s = 0.0;
				for (unsigned k = 0; k < 4; ++k)
				{
					if (k != 3)
					{
						s += data[i + k * 3] * data[k + j * 3];
					}
					else if (k == j)
					{
						s += data[i + k * 3];
					}
				}
				temp.data[i + j * 3] = s;
			}
		}

		return temp;
	}
#ifdef USE_FIXEDPOINT
	vec3 operator*(const vec3& v) const
	{
#pragma HLS INLINE
		vec3 temp;
		temp.data[0] = (myTypeReduced)data[0] * (myTypeReduced)v.data[0] + (myTypeReduced)data[3] * (myTypeReduced)v.data[1] + (myTypeReduced)data[6] * (myTypeReduced)v.data[2] + (myTypeReduced)data[9];
		temp.data[1] = (myTypeReduced)data[1] * (myTypeReduced)v.data[0] + (myTypeReduced)data[4] * (myTypeReduced)v.data[1] + (myTypeReduced)data[7] * (myTypeReduced)v.data[2] + (myTypeReduced)data[10];
		temp.data[2] = (myTypeReduced)data[2] * (myTypeReduced)v.data[0] + (myTypeReduced)data[5] * (myTypeReduced)v.data[1] + (myTypeReduced)data[8] * (myTypeReduced)v.data[2] + (myTypeReduced)data[11];
		return temp;
	}
#else
	vec3 operator*(const vec3& v) const
	{
#pragma HLS INLINE
		vec3 temp;
		temp.data[0] = data[0] * v.data[0] + data[3] * v.data[1] + data[6] * v.data[2] + data[9];
		temp.data[1] = data[1] * v.data[0] + data[4] * v.data[1] + data[7] * v.data[2] + data[10];
		temp.data[2] = data[2] * v.data[0] + data[5] * v.data[1] + data[8] * v.data[2] + data[11];
		return temp;
	}
#endif

	vec3 Transform(const vec3& v)
	{
#pragma HLS INLINE
		return *this * v;
	}

#ifdef USE_FIXEDPOINT
	vec3 TransformDir(const vec3& v) const
	{
#pragma HLS INLINE
		vec3 temp;
		temp.data[0] = (myTypeReduced)data[0] * (myTypeReduced)v.data[0] + (myTypeReduced)data[3] * (myTypeReduced)v.data[1] + (myTypeReduced)data[6] * (myTypeReduced)v.data[2];
		temp.data[1] = (myTypeReduced)data[1] * (myTypeReduced)v.data[0] + (myTypeReduced)data[4] * (myTypeReduced)v.data[1] + (myTypeReduced)data[7] * (myTypeReduced)v.data[2];
		temp.data[2] = (myTypeReduced)data[2] * (myTypeReduced)v.data[0] + (myTypeReduced)data[5] * (myTypeReduced)v.data[1] + (myTypeReduced)data[8] * (myTypeReduced)v.data[2];
		return temp;
	}
#else
	vec3 TransformDir(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = data[0] * v.data[0] + data[3] * v.data[1] + data[6] * v.data[2];
		temp.data[1] = data[1] * v.data[0] + data[4] * v.data[1] + data[7] * v.data[2];
		temp.data[2] = data[2] * v.data[0] + data[5] * v.data[1] + data[8] * v.data[2];
		return temp;
	}
#endif


#ifdef USE_FIXEDPOINT
	vec3 TransposeTransformDir(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = (myTypeReduced)data[0] * (myTypeReduced)v.data[0] + (myTypeReduced)data[1] * (myTypeReduced)v.data[1] + (myTypeReduced)data[2] * (myTypeReduced)v.data[2];
		temp.data[1] = (myTypeReduced)data[3] * (myTypeReduced)v.data[0] + (myTypeReduced)data[4] * (myTypeReduced)v.data[1] + (myTypeReduced)data[5] * (myTypeReduced)v.data[2];
		temp.data[2] = (myTypeReduced)data[6] * (myTypeReduced)v.data[0] + (myTypeReduced)data[7] * (myTypeReduced)v.data[1] + (myTypeReduced)data[8] * (myTypeReduced)v.data[2];
		return temp;
	}
#else
	vec3 TransposeTransformDir(const vec3& v) const
	{
		vec3 temp;
		temp.data[0] = data[0] * v.data[0] + data[1] * v.data[1] + data[2] * v.data[2];
		temp.data[1] = data[3] * v.data[0] + data[4] * v.data[1] + data[5] * v.data[2];
		temp.data[2] = data[6] * v.data[0] + data[7] * v.data[1] + data[8] * v.data[2];
		return temp;
	}
#endif

#ifdef USE_FIXEDPOINT
	myType TransposeTransformDir(const vec3& v, unsigned i) const
	{
#pragma HLS INLINE
		myType temp;

		switch(i)
		{
		case 0:
			temp = (myTypeReduced)data[0] * (myTypeReduced)v.data[0] +
				   (myTypeReduced)data[1] * (myTypeReduced)v.data[1] +
				   (myTypeReduced)data[2] * (myTypeReduced)v.data[2];
			break;
		case 1:
			temp = (myTypeReduced)data[3] * (myTypeReduced)v.data[0] +
			       (myTypeReduced)data[4] * (myTypeReduced)v.data[1] +
				   (myTypeReduced)data[5] * (myTypeReduced)v.data[2];
			break;
		default:
			temp = (myTypeReduced)data[6] * (myTypeReduced)v.data[0] +
				   (myTypeReduced)data[7] * (myTypeReduced)v.data[1] +
				   (myTypeReduced)data[8] * (myTypeReduced)v.data[2];
			break;
		}

		return temp;
	}
#endif

#define D3(n1, n2, n3) (data[n1]*data[n2]*data[n3])
#define D2(n1, n2) (data[n1]*data[n2])
	myType Det() const
	{
		return D3(0, 4, 8) + D3(3, 7, 2) + D3(6, 1, 5) - (D3(6, 4, 2) + D3(3, 1, 8) + D3(0, 7, 5));
	}

	mat4 Inverse() const
	{
		myType det = Det();
		if (det == (myType)0.0) return *this;

		mat4 temp;
		myType invDet = (myType)1.0/det;

		temp.data[0] = D2(4, 8) - D2(7, 5);
		temp.data[1] = D2(7, 2) - D2(1, 8);
		temp.data[2] = D2(1, 5) - D2(4, 2);

		temp.data[3] = D2(6, 5) - D2(3, 8);
		temp.data[4] = D2(0, 8) - D2(6, 2);
		temp.data[5] = D2(3, 2) - D2(0, 5);

		temp.data[6] = D2(3, 7) - D2(6, 4);
		temp.data[7] = D2(6, 1) - D2(0, 7);
		temp.data[8] = D2(0, 4) - D2(3, 1);

		temp.data[9] = D3(9, 7, 5) + D3(6, 4, 11) + D3(3, 10, 8) - (D3(3, 7, 11) + D3(6, 10, 5) + D3(9, 4, 8));
		temp.data[10] = D3(0, 7, 11) + D3(6, 10, 2) + D3(9, 1, 8) - (D3(9, 7, 2) + D3(6, 1, 11) + D3(0, 10, 8));
		temp.data[11] = D3(9, 4, 2) + D3(3, 1, 11) + D3(0, 10, 5) - (D3(0, 4, 11) + D3(3, 10, 2) + D3(9, 1, 5));

		return temp * invDet;
	}
#undef D3
#undef D2

	void IdentityMatrix()
	{
		for (unsigned i = 0; i < 12; ++i)
		{
			data[i] = (i % 4 == 0)?1.0:0.0;
		}
	}

	void TranslationMatrix(const vec3& v)
	{
		for (unsigned i = 0; i < 9; ++i)
		{
			data[i] = (i % 4 == 0)?1.0:0.0;
		}
		data[9] = v.data[0];	data[10] = v.data[1];	data[11] = v.data[2];
	}

	void ScaleMatrix(const vec3& v)
	{
		for (unsigned i = 0; i < 4; ++i)
		{
			for (unsigned j = 0; j < 3; ++j)
			{
				if (i == j) data[j + i * 3] = v.data[j];
				else data[j + i * 3] = 0.0;
			}
		}
	}

	void RotationMatrix(myType rad, const vec3& v)
	{
		myType mag, s, c;
		myType xx, yy, zz, xy, yz, zx, xs, ys, zs, one_c;
		vec3 rotVec;

#ifdef USE_FIXEDPOINT
		s = myType(hls::sin((float)rad));
		c = myType(hls::cos((float)rad));
#else
		s = myType(std::sin(rad));
		c = myType(std::cos(rad));
#endif
		mag = v.Magnitude();

		// Identity matrix
		if (mag < (myType)0.001) {
			this->IdentityMatrix();
			return;
		}

		// Rotation matrix is normalized
		rotVec = v / mag;

		xx = rotVec.data[0] * rotVec.data[0];
		yy = rotVec.data[1] * rotVec.data[1];
		zz = rotVec.data[2] * rotVec.data[2];
		xy = rotVec.data[0] * rotVec.data[1];
		yz = rotVec.data[1] * rotVec.data[2];
		zx = rotVec.data[2] * rotVec.data[0];
		xs = rotVec.data[0] * s;
		ys = rotVec.data[1] * s;
		zs = rotVec.data[2] * s;
		one_c = (myType)1.0 - c;

#define M(row,col)  data[col*3+row]

	M(0,0) = (one_c * xx) + c;	M(0,1) = (one_c * xy) - zs; M(0,2) = (one_c * zx) + ys;	M(0,3) = 0.0;
	M(1,0) = (one_c * xy) + zs; M(1,1) = (one_c * yy) + c;	M(1,2) = (one_c * yz) - xs;	M(1,3) = 0.0;
	M(2,0) = (one_c * zx) - ys; M(2,1) = (one_c * yz) + xs; M(2,2) = (one_c * zz) + c;  M(2,3) = 0.0;
//	M(3,0) = 0.0;				M(3,1) = 0.0;				M(3,2) = 0.0;				M(3,3) = 1.0;

#undef M
	}


//	void DecodeFromStream(const myType* stream)
//	{
//		for (unsigned i = 0; i < 16; ++i)
//			data[i] = *(stream + i);
//	}
};

struct shadeRec
{
	bool isHit;
	myType t;
	vec3 normal;
	vec3 hitPoint;
	vec3 localHitPoint;
	vec3 color;

	shadeRec() : isHit(0), t(HUGE_REAL_VAL) {}
};

struct light
{
#ifndef USE_FIXEDPOINT
	myType radiance;
	myType color_r, color_g, color_b;
	myType position_x, position_y, position_z;

	myType k0, k1, k2;
#else
	myType data[10];
#endif
};

struct material
{
#ifndef USE_FIXEDPOINT
	myType ambient_color_r, ambient_color_g, ambient_color_b;
	myType ambient_k, ambient_exp;

	myType diffuse_color_r, diffuse_color_g, diffuse_color_b;
	myType diffuse_k, diffuse_exp;

	myType specular_color_r, specular_color_g, specular_color_b;
	myType specular_k, specular_exp;
#else
	myType data[15];
#endif
};


/*
 * TODO Update definition...
 */
// Represents a single instruction, its type and up to 4 parameters (the last parameter must be a local variable, either scalar or vector) according to instruction type
struct Instruction {
	unsigned 	it8_wr12_idx;
	unsigned 	r012_r112_r212;
};

struct ExternalRegister
{
	unsigned short reg;
	myType x, y, z;
};

myType func(Instruction *instructionList, unsigned instructionListSize,
		    ExternalRegister *externalRegisterList, unsigned externalRegisterListSize,
			myType* rayData, unsigned rayDataSize,
							mat4 *transformationMatrices,
							material* sceneMaterials,
		//					unsigned sceneObjectsNum,
							light* sceneLights,
							unsigned sceneLightsNum,
							pixelColorType *pixelColor);

#endif
