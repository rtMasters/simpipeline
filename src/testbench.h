#ifndef TESTBENCH__H
#define TESTBENCH__H

#include "main.h"

#define Vx 0
#define Vy 1
#define Vz 2
#define V  3

#define CONVERT_TO_IT8_WR12_IDX(it, wr, idxWr, idxR0, idxR1) ((it << 24) + (wr << 12) + (idxWr << 8) + ((idxR0) << 4) + idxR1)
#define CONVERT_TO_R016_R116(r0, r1) (((short)r0 << 16) + ((short)r1 & 0xFFFF))

#ifdef USE_FIXEDPOINT

void WriteInstructionIntoStream(Instruction *instructionList, const Instruction& instruction, unsigned& i);
void WriteExternalRegister(ExternalRegister* externalRegisterList, const ExternalRegister& externalRegister, unsigned& i);
void WriteMatrix(mat4* mats, const mat4& mat, unsigned i);
void GenerateDivInstructions(Instruction* instructionList, unsigned operationalRegister, short int numRegister, short int denRegister,
																						 unsigned char numRegisterIdx, unsigned char denRegisterIdx, unsigned& instructionListSize, unsigned iterations = 1);
void GenerateSqrtInstructions(Instruction* instructionList, unsigned operationalRegister, short int valInRegister, unsigned char valInRegisterIdx, unsigned& instructionListSize, bool nonInverse = false, unsigned iterations = 2);
void GenerateRayTransformationInstructions(Instruction* instructionList, unsigned& instructionListSize);
void GenerateSphereHitInstructions(Instruction* instructionList, unsigned& instructionListSize);

#else

#endif

#endif
