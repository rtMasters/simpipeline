#include <iostream>

#include "testbench.h"

using namespace std;

void WriteInstructionIntoStream(Instruction *instructionList, const Instruction& instruction, unsigned& i)
{
	memcpy(instructionList + (i++), &instruction, sizeof(Instruction));
}

void WriteExternalRegister(ExternalRegister* externalRegisterList, const ExternalRegister& externalRegister, unsigned& i)
{
	memcpy(externalRegisterList + (i++), &externalRegister, sizeof(ExternalRegister));
}

void WriteMatrix(mat4* mats, const mat4& mat, unsigned i)
{
	memcpy(mats + i, &mat, sizeof(mat4));
}

// The result is stored in < Vy > component of < operationalRegister >
void GenerateDivInstructions(Instruction* instructionList, unsigned operationalRegister, short int numRegister, short int denRegister,
																						 unsigned char numRegisterIdx, unsigned char denRegisterIdx, unsigned& instructionListSize, unsigned iterations)
{
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(PREDIV, operationalRegister, V, numRegisterIdx, denRegisterIdx), CONVERT_TO_R016_R116(numRegister, denRegister)}, instructionListSize);

	for (unsigned i = 0; i < iterations; ++i)
	{
		//...
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, LV_NONE, Vx, Vx, Vz), CONVERT_TO_R016_R116(operationalRegister, operationalRegister)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, LV_NONE, Vx, Vx, Vx), CONVERT_TO_R016_R116(LV_V3_ONE_TWO_THREE, -LV_NONE)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, LV_NONE, Vx, Vz, Vx), CONVERT_TO_R016_R116(operationalRegister, LV_NONE)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, operationalRegister, Vz, Vz, Vx), CONVERT_TO_R016_R116(operationalRegister, LV_NONE)}, instructionListSize);
	}
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, operationalRegister, Vy, Vy, Vz), CONVERT_TO_R016_R116(operationalRegister, operationalRegister)}, instructionListSize);
}

// The result is stored in < Vy > component of < operationalRegister >
void GenerateSqrtInstructions(Instruction* instructionList, unsigned operationalRegister, short int valInRegister, unsigned char valInRegisterIdx, unsigned& instructionListSize, bool nonInverse, unsigned iterations)
{
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(PRESQRT, operationalRegister, V, valInRegisterIdx, 0), CONVERT_TO_R016_R116(valInRegister, 0)}, instructionListSize);

	for (unsigned i = 0; i < iterations; ++i)
	{
		if (i != 0) WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, operationalRegister, Vz, Vy, Vx), CONVERT_TO_R016_R116(operationalRegister, operationalRegister)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, operationalRegister, Vz, Vy, Vz), CONVERT_TO_R016_R116(operationalRegister, operationalRegister)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, operationalRegister, Vz, Vz, Vz), CONVERT_TO_R016_R116(LV_V3_HALF_QUATER_THREEHALFS, -operationalRegister)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, operationalRegister, Vy, Vy, Vz), CONVERT_TO_R016_R116(operationalRegister, operationalRegister)}, instructionListSize);
	}

	if (nonInverse)
	{
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, operationalRegister, Vy, Vy, Vx), CONVERT_TO_R016_R116(operationalRegister, operationalRegister)}, instructionListSize);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MUL, operationalRegister, Vy, Vy, Vy), CONVERT_TO_R016_R116(operationalRegister, LV_V3_ONE_TWO_THREE)}, instructionListSize);
	}
}

void GenerateRayTransformationInstructions(Instruction* instructionList, unsigned& instructionListSize)
{
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MMULV, RAY_ORIGIN_TRANSFORMED, V, 0, 0), CONVERT_TO_R016_R116(TRANS_MAT_0_INV, RAY_ORIGIN)}, instructionListSize);
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MMULDIR, RAY_DIRECTION_TRANSFORMED, V, 0, 0), CONVERT_TO_R016_R116(TRANS_MAT_0_INV, RAY_DIRECTION)}, instructionListSize);
	//there should be normalization in principle...
}

void GenerateSphereHitInstructions(Instruction* instructionList, unsigned& instructionListSize)
{
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOV, LV_V3a, Vx, Vx, 0), CONVERT_TO_R016_R116(LV_V3_ONE_TWO_THREE, 0)}, instructionListSize);
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(DOT, LV_V3a, Vy, V, V), CONVERT_TO_R016_R116(RAY_ORIGIN_TRANSFORMED, RAY_DIRECTION_TRANSFORMED)}, instructionListSize);
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(DOT, LV_V3a, Vz, V, V), CONVERT_TO_R016_R116(RAY_ORIGIN_TRANSFORMED, RAY_ORIGIN_TRANSFORMED)}, instructionListSize);
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, LV_V3a, Vz, Vz, Vx), CONVERT_TO_R016_R116(LV_V3a, -LV_V3_ONE_TWO_THREE)}, instructionListSize);
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(DELTA, LV_V3b, Vx, V, 0), CONVERT_TO_R016_R116(LV_V3a, 0)}, instructionListSize);

	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(IF_GE, 0, 0, Vx, Vx), CONVERT_TO_R016_R116(LV_V3b, LV_V3_ZERO_EPS_DIST)}, instructionListSize);

		GenerateSqrtInstructions(instructionList, LV_V3c, LV_V3b, Vx, instructionListSize, true);
		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, LV_V3_ZERO_EPS_DIST, Vz, Vy, Vy), CONVERT_TO_R016_R116(-LV_V3a, -LV_V3c)}, instructionListSize);

		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(IF_GE, 0, 0, Vz, Vy), CONVERT_TO_R016_R116(LV_V3_ZERO_EPS_DIST, LV_V3_ZERO_EPS_DIST)}, instructionListSize);

			WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOV, BESTHIT_COLOR, Vz, Vz, 0), CONVERT_TO_R016_R116(LV_V3_ZERO_EPS_DIST, 0)}, instructionListSize);

		WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ELSE, 0, 0, 0, 0), CONVERT_TO_R016_R116(0, 0)}, instructionListSize);

			WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, LV_V3_ZERO_EPS_DIST, Vz, Vy, Vy), CONVERT_TO_R016_R116(-LV_V3a, LV_V3c)}, instructionListSize);

			WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(IF_GE, 0, 0, Vz, Vy), CONVERT_TO_R016_R116(LV_V3_ZERO_EPS_DIST, LV_V3_ZERO_EPS_DIST)}, instructionListSize);

				WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOV, BESTHIT_COLOR, Vz, Vz, 0), CONVERT_TO_R016_R116(LV_V3_ZERO_EPS_DIST, 0)}, instructionListSize);

			WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ENDIF, 0, 0, 0, 0), CONVERT_TO_R016_R116(0, 0)}, instructionListSize);

	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ENDIF, 0, 0, 0, 0), CONVERT_TO_R016_R116(0, 0)}, instructionListSize);

	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOV, BESTHIT_COLOR, Vy, Vy, 0), CONVERT_TO_R016_R116(LV_V3c, 0)}, instructionListSize);

	// check sqrt for numbers less than 1
//	GenerateSqrtInstructions(instructionList, LV_V3c, LV_V3_HALF_QUATER_THREEHALFS, Vz, instructionListSize, true, 2);
	GenerateDivInstructions(instructionList, LV_V3c, -LV_V3_ONE_TWO_THREE, LV_V3_HALF_QUATER_THREEHALFS, Vz, Vz, instructionListSize, 1);
	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOV, BESTHIT_COLOR, Vx, Vy, 0), CONVERT_TO_R016_R116(LV_V3c, 0)}, instructionListSize);

//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOVV, BESTHIT_COLOR, V, V, 0), CONVERT_TO_R016_R116(LV_V3a, 0)}, instructionListSize);

//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOVV, BESTHIT_COLOR, V, 0, 0), CONVERT_TO_R016_R116(LV_V3a, 0)}, instructionListSize);
//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOVV, BESTHIT_COLOR, V, 0, 0), CONVERT_TO_R016_R116(RAY_DIRECTION_TRANSFORMED, 0)}, instructionListSize);
}


int main()
{
	Instruction *instructionList = new Instruction[ASM_MAX_INSTRUCTION_COUNT];
	unsigned instructionListSize = 0;

	ExternalRegister *externalRegisterList = new ExternalRegister[MAX_EXTERNAL_REGISTER_NUM];
	unsigned externalRegisterListSize = 0;

	myType* rayData = new myType[6];
	unsigned rayDataSize = 1;

	mat4 *transformationMatrices = new mat4[MAX_SCENE_OBJECTS * 2];
	material* sceneMaterials = 0;

	light* sceneLights = 0;
	unsigned sceneLightsNum = 0;
	pixelColorType *pixelColor = new pixelColorType[3];

	//-----------------

//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MOVV, BESTHIT_COLOR, V, 0, 0), CONVERT_TO_R016_R116(LV_VEXT0, -LV_V3a)}, instructionListSize);
//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MMULV, BESTHIT_COLOR, V, 0, 0), CONVERT_TO_R016_R116(TRANS_MAT_0, -BESTHIT_COLOR)}, instructionListSize);
//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(ADD, BESTHIT_COLOR, Vx, Vx, Vz), CONVERT_TO_R016_R116(BESTHIT_COLOR, LV_V3_ONE_TWO_THREE)}, instructionListSize);
//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(MULV, BESTHIT_COLOR, V, V, Vy), CONVERT_TO_R016_R116(BESTHIT_COLOR, -LV_V3_ONE_TWO_THREE)}, instructionListSize);
//	WriteInstructionIntoStream(instructionList, Instruction{CONVERT_TO_IT8_WR12_IDX(DOT, BESTHIT_COLOR, Vx, V, V), CONVERT_TO_R016_R116(BESTHIT_COLOR, BESTHIT_COLOR)}, instructionListSize);

	GenerateRayTransformationInstructions(instructionList, instructionListSize);
	GenerateSphereHitInstructions(instructionList, instructionListSize);

	WriteExternalRegister(externalRegisterList, ExternalRegister{LV_VEXT0, (myType)-1.0, (myType)1.0, (myType)1.0}, externalRegisterListSize);

	rayData[0] = 0.0; rayData[1] = 0.0; rayData[2] = -1.0;
	rayData[3] = 0.0; rayData[4] = 0.0; rayData[5] = 0.0;

	mat4 tmpTransf;
	tmpTransf.IdentityMatrix();
	tmpTransf.data[9]  = 0.0;
	tmpTransf.data[11] = -2.5;
	WriteMatrix(transformationMatrices, tmpTransf, 0);

	tmpTransf.data[9]  = -tmpTransf.data[9];
	tmpTransf.data[11] = -tmpTransf.data[11];
	WriteMatrix(transformationMatrices, tmpTransf, 1);

	//-----------------

	func(instructionList, instructionListSize,
		 externalRegisterList, externalRegisterListSize,
		 rayData, rayDataSize,
		 transformationMatrices,
		 sceneMaterials,
		 sceneLights, sceneLightsNum,
		 pixelColor);

	cout << "Ilosc instrukcji: " << instructionListSize << endl;
	for (unsigned i = 0; i < 3; ++i)
	{
		cout << pixelColor[i].to_float() << endl;
	}

//	myType min1 = -1;
//	cout << min1.countLeadingZeros() << endl;

	delete[] instructionList;
	delete[] externalRegisterList;
	delete[] rayData;
	delete[] transformationMatrices;

	delete[] pixelColor;

	return 0;
}
