#include "main.h"
#include "hls_stream.h"

struct InstructionExtracted
{
	unsigned char it;
	unsigned short writeRegister;
	unsigned char paramType;

	short int l1;
	short int l2;

	vec3 vec;

	InstructionExtracted operator=(const Instruction inst)
	{
		it = inst.it1wrReg2pt1 >> 24;
		writeRegister = (inst.it1wrReg2pt1 & 0xFFFF00) >> 8;
		paramType = inst.it1wrReg2pt1 & 0xFF;

		l1 = (inst.l22 >> 16);
		l2 = (inst.l22 & 0xFFFF);

		vec.data[0] = inst.x;
		vec.data[1] = inst.y;
		vec.data[2] = inst.z;

		return *this;
	}
};

//------------------------------------------------------------------------------------

void LoadRay(myType* rays, vec3* ray, unsigned rayNum)
{
#pragma HLS PIPELINE
//#pragma HLS DATAFLOW
	/*if (instructionNum == 0) */memcpy(ray, rays + rayNum * sizeof(vec3) * 2, sizeof(vec3) * 2);
}

void LoadInstruction(Instruction* instructionList, InstructionExtracted* instructionOut, unsigned instructionNum)
{
#pragma HLS PIPELINE
	static Instruction instTmp;
	memcpy(instructionList + instructionNum * sizeof(Instruction), &instTmp, sizeof(Instruction));

	*instructionOut = instTmp;
}

void PrepareLocalRegisters(unsigned instructionNum,
						   vec3* vecs, vec3* vecsBuf,
//						   myType* scalars, myType* scalarsBuf,
						   vec3* rayTmp,
//						   unsigned short writeRegister,
						   vec3* result,
						   InstructionExtracted* instructionIn,
						   unsigned char& it, unsigned short& writeRegister,
						   vec3* tmpVec0, vec3* tmpVec1)
{
#pragma HLS PIPELINE II=1

//	PrepareLocalRegisters_label0:for (unsigned i = 0; i < TRANS_MAT_0 - LV_V3a; ++i)
//	{
//		if (instructionNum == 0 && i == RAY_DIRECTION - LV_V3a) vecs[i] = rayTmp[0];
//		else if (instructionNum == 0 && i == RAY_ORIGIN - LV_V3a) vecs[i] = rayTmp[1];
//		else if (instructionNum == 0 && i == BESTHIT_COLOR - LV_V3a)
//		{
//			vecs[i].data[0] = 0.0;
//			vecs[i].data[1] = 0.0;
//			vecs[i].data[2] = 0.0;
//		}
//		else
//		{
//			if (i + LV_V3a == writeRegister)
//			{
//				vecs[i] = *result;
//			}
//			else vecs[i] = vecsBuf[i];
//		}
//	}
//	PrepareLocalRegisters_label1:for (unsigned i = 0; i < LV_V3a_0; ++i)
//	{
//		if (instructionNum == 0 && i == BESTHIT_ISHIT) scalars[i] = 0.0;
//		else if (instructionNum == 0 && i == BESTHIT_DIST) scalars[i] = HUGE_REAL_VAL;
//		else scalars[i] = scalarsBuf[i];
//	}

	it = instructionIn->it;
	writeRegister = instructionIn->writeRegister;

	*tmpVec0 = vecsBuf[instructionIn->it];
	*tmpVec1 = vecsBuf[instructionIn->writeRegister];
}

void DecodeInstruction(InstructionExtracted* instructionExtracted,
						unsigned char& it, unsigned short& writeRegister,
						vec3* vecs,
						vec3* tmpVec0, vec3* tmpVec1)
{
#pragma HLS PIPELINE
	it = instructionExtracted->it;
	writeRegister = instructionExtracted->writeRegister;

//	*tmpVec0 = vecs[0];
//	*tmpVec1 = vecs[2];
}

void LoadInstructionData(unsigned instructionNum,
						Instruction* rawInstructions,
						unsigned char& it, unsigned short& writeRegister,
						vec3* vecs, vec3* vecsCopy,
//						myType* scalars, myType* scalarsCopy,
						vec3* tmpVec0, vec3* tmpVec1)
{
#pragma HLS PIPELINE

	static InstructionExtracted instructionExtracted;
	instructionExtracted = rawInstructions[instructionNum];

	it = instructionExtracted.it;
	writeRegister = instructionExtracted.writeRegister;

//	for (unsigned i = 0; i < TRANS_MAT_0 - LV_V3a; ++i)
//	{
//#pragma HLS PIPELINE
//		vecsCopy[i] = vecs[i];
//	}
//	for (unsigned i = 0; i < LV_V3a_0; ++i)
//	{
//#pragma HLS PIPELINE
//		scalarsCopy[i] = scalars[i];
//	}
	*tmpVec0 = vecs[it];
	*tmpVec1 = vecs[writeRegister];
}

void Eval(unsigned char instructionType,
		 vec3* tmpVec0, vec3* tmpVec1,
		 vec3* result)
{
#pragma HLS PIPELINE
	switch(instructionType)
	{
	case 0:
		*result = *tmpVec0 + *tmpVec1;
		break;
	case 1:
		result->data[0] = *tmpVec0 * *tmpVec1;
	}
}

//void Update(unsigned rayNum, unsigned instructionNum, unsigned instructionListSize,
//		    vec3* vecsCopy, vec3* vecs,
//			myType* scalarsCopy, myType* scalars,
//			unsigned short writeRegister, vec3* result,
//			pixelColorType *pixelColor)
//{
//#pragma HLS PIPELINE
//	static pixelColorType colorBuff[3];
//	for (unsigned i = LV_V3a; i < TRANS_MAT_0; ++i)
//	{
//		if (i == writeRegister) vecs[i] = *result;
//		else vecs[i] = vecsCopy[i];
//	}
//	for (unsigned i = 0; i < LV_V3a_0; ++i)
//	{
//		if (i == writeRegister) scalars[i] = result->data[0];
//		else scalars[i] = scalarsCopy[i];
//	}
//
////	if (instructionNum + 1 == instructionListSize)
////	{
////		colorBuff[0] = (pixelColorType)(vecsCopy[BESTHIT_COLOR - LV_V3a].data[0] * 255);
////		colorBuff[1] = (pixelColorType)(vecsCopy[BESTHIT_COLOR - LV_V3a].data[1] * 255);
////		colorBuff[2] = (pixelColorType)(vecsCopy[BESTHIT_COLOR - LV_V3a].data[2] * 255);
////
////		memcpy(pixelColor + rayNum * sizeof(pixelColorType) * 3, colorBuff, sizeof(pixelColorType) * 3);
////	}
//}

void ProcessInstructions(unsigned rayNum, unsigned rayDataSize, unsigned instructionNum, unsigned instructionListSize,
						myType* rayData,
						Instruction* instructionList,
						myType* scalars,
						vec3* vecs,
						mat4* mats,
						material* materials,
						pixelColorType *pixelColor,
						vec3* vecsCopyIn)
{
#pragma HLS DATAFLOW
//#pragma HLS PIPELINE
	static vec3 rayTmp[2];
//	static vec3 vecsBuf[TRANS_MAT_0 - LV_V3a];
//	static vec3 vecsCopy[TRANS_MAT_0 - LV_V3a];
//	static myType scalarsCopy[LV_V3a_0];
//	static myType scalarsBuf[LV_V3a_0];

	static unsigned char it;
	static unsigned short writeRegister;

	static vec3 tmpVec0, tmpVec1, result;

	static InstructionExtracted instructionExtracted, instructionOut;

	LoadRay(rayData, rayTmp, instructionNum);

	LoadInstruction(instructionList, &instructionExtracted, instructionNum);

	PrepareLocalRegisters(instructionNum,
			vecs, vecsCopyIn,
//			scalars, scalarsBuf,
			rayTmp,
//			writeRegister,
			&result,
			&instructionExtracted, it, writeRegister,
			&tmpVec0, &tmpVec1);

//	DecodeInstruction(&instructionOut, it, writeRegister,
//					  vecs,
//					  &tmpVec0, &tmpVec1);

//	LoadInstructionData(instructionNum, rawInstructions,
//			it, writeRegister,
//			vecs, vecsCopy,
//			scalars, scalarsCopy,
//			&tmpVec0, &tmpVec1);
//
	Eval(it, &tmpVec0, &tmpVec1, &result);

//	LoadRay(rayData, rayTmp, instructionNum);

//	Update(rayNum, instructionNum, instructionListSize,
//			vecsCopy, vecs,
//			scalarsCopy, scalars,
//			writeRegister, &result,
//			pixelColor);

//	for (unsigned i = 0; i < TRANS_MAT_0 - LV_V3a; ++i )
//	{
//		vecsBuf[i] = vecs[i]; //simplified...
//	}
}

//------------------------------------------------------------------------------------


myType func(Instruction *instructionList,
							unsigned instructionListSize,
							myType *rayData,
							unsigned rayDataSize,
							mat4 *transformationMatrices,
							material* sceneMaterials,
		//					unsigned sceneObjectsNum,
							light* sceneLights,
							unsigned sceneLightsNum,
							pixelColorType *pixelColor)
{
	myType out = 0;

#pragma HLS INTERFACE s_axilite port=sceneLightsNum bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=sceneLights bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=sceneLights offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=sceneMaterials bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=sceneMaterials offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=rayDataSize bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=rayData bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=rayData offset=slave bundle=MAXI_IN

//#pragma HLS INTERFACE s_axilite port=sceneObjectsNum bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=transformationMatrices bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=transformationMatrices offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=pixelColor bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=pixelColor offset=slave bundle=MAXI_OUT
//#pragma HLS DATA_PACK variable=outData field_level
#pragma HLS INTERFACE s_axilite port=instructionListSize bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=instructionList bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=instructionList offset=slave bundle=MAXI_IN
//#pragma HLS DATA_PACK variable=instructionList

#pragma HLS INTERFACE s_axilite port=return bundle=AXI_LITE_1

	myType scalars[LV_V3a_0];			 // additional for eps and distance along ray direction (t)
	scalars[EPS] = RAY_HIT_EPS;

	vec3 vecs[TRANS_MAT_0 - LV_V3a];  // two additional values reserved for ray data + 2 for ShadeRec data
	vec3 vecsInOut[TRANS_MAT_0 - LV_V3a];

	mat4 mats[MAX_SCENE_OBJECTS * 2];
	memcpy(mats, transformationMatrices, sizeof(mat4) * MAX_SCENE_OBJECTS * 2);

	material materials[MAX_SCENE_OBJECTS];
	memcpy(materials, sceneMaterials, sizeof(material) * MAX_SCENE_OBJECTS);

//	light lights[MAX_SCENE_LIGHTS];
//	memcpy(lights, sceneLights, sizeof(light) * MAX_SCENE_LIGHTS);
//	RTSoftProcessor_GetLights:for (unsigned i = 0; i < MAX_SCENE_LIGHTS; ++i)
//	{
//#pragma HLS PIPELINE
//		if (i < sceneLightsNum)
//		{
//			scalars[LIGHT0_RADIANCE + i] = lights[i].data[0];
//
//			vecs[LIGHT0_COLOR + i - LV_V3a].data[0] = lights[i].data[1];
//			vecs[LIGHT0_COLOR + i - LV_V3a].data[1] = lights[i].data[2];
//			vecs[LIGHT0_COLOR + i - LV_V3a].data[2] = lights[i].data[3];
//
//			vecs[LIGHT0_POSITION + i - LV_V3a].data[0] = lights[i].data[4];
//			vecs[LIGHT0_POSITION + i - LV_V3a].data[1] = lights[i].data[5];
//			vecs[LIGHT0_POSITION + i - LV_V3a].data[2] = lights[i].data[6];
//
//			vecs[LIGHT0_K + i - LV_V3a].data[0] = lights[i].data[7];
//			vecs[LIGHT0_K + i - LV_V3a].data[1] = lights[i].data[8];
//			vecs[LIGHT0_K + i - LV_V3a].data[2] = lights[i].data[9];
//		}
//	}

    int actualLvl = -1;
    bool conditionResult[ASM_MAX_CONDITIONAL_DEPTH];
    bool conditionType[ASM_MAX_CONDITIONAL_DEPTH]; //0 - IF, 1 - ELSE
    bool canProcess[ASM_MAX_CONDITIONAL_DEPTH + 1];

//	shadeRec hitResult;//, bestHit;

	vec3 tempVec0, tempVec1;

	Instruction instINtemp[ASM_MAX_INSTRUCTION_COUNT];
	InstructionExtracted instructionsListExtracted[ASM_MAX_INSTRUCTION_COUNT];
	memcpy(instINtemp, instructionList, sizeof(Instruction) * ASM_MAX_INSTRUCTION_COUNT);
	for (unsigned i = 0; i < ASM_INSTRUCTION_SIZE; ++i)
	{
		if (i < instructionListSize) instructionsListExtracted[i] = instINtemp[i];
	}

	vec3 rayVecs[RAY_DATA_AND_OUTPUT_COLOR_PRELOAD_SIZE * 2];

	unsigned rayDataLoadingCounter = 0;
	unsigned rayDataLoadingNum = 0;

	pixelColorType colorBuff[RAY_DATA_AND_OUTPUT_COLOR_PRELOAD_SIZE * 3];

	unsigned visibleObjIdx;

	// power variables
	myType powResult;
	myType base;
	unsigned exp;

	func_label2:for (unsigned rayNum = 0; rayNum < rayDataSize; ++rayNum)
	{
#pragma HLS LOOP_TRIPCOUNT min=1 max=1
		func_label0:for (unsigned instructionNum = 0; instructionNum < instructionListSize; ++instructionNum)
		{
#pragma HLS LOOP_TRIPCOUNT min=10 max=10
//#pragma HLS DATAFLOW
//#pragma HLS PIPELINE
			ProcessInstructions(rayNum, rayDataSize, instructionNum, instructionListSize,
					 rayData,
					 instructionList,
					 scalars,
					 vecs,
					 mats,
					 materials,
					 pixelColor,
					 vecsInOut);
			vecs[rayNum].data[0] = rayNum;
		}
	}

	return out;
}
