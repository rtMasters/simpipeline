// vec.operator-() - usunac rozdzielenie na tworzenie i zapisywanie obiektu tymczasowego, mozna np. dodajac konstruktor vecs(x, y, z)
// vecs -> RESOURCE -> BRAM_L2P ()

#include "main.h"
#include "hls_stream.h"

struct InstructionExtracted
{
	unsigned char it;					// instruction type 								->	8 bits
	short int wr;						// write register									-> 12 bits
	unsigned char idxWr, idx0, idx1;	// write register, input1, input2 idxs (x, y, z)	-> 3 x 4 bits

	short int r0;						// input0											-> 16 bits (1 (signed) + 15)
	short int r1;						// input1											-> 16 bits (1 (signed) + 15)

	InstructionExtracted operator=(const Instruction inst)
	{
#pragma HLS INLINE
//#pragma HLS PIPELINE
		it = inst.it8_wr12_idx >> 24;
		wr = (inst.it8_wr12_idx & 0x00FFF000) >> 12;

		idxWr = (inst.it8_wr12_idx & 0xF00) >> 8;
		idx0  = (inst.it8_wr12_idx & 0x0F0) >> 4;
		idx1  = (inst.it8_wr12_idx & 0x00F);

		r0 = (inst.r012_r112_r212 & 0xFFFF0000) >> 16;
		r1 = (inst.r012_r112_r212 & 0x0000FFFF);

		return *this;
	}
};

struct InstructionFlow
{
    int actualLvl;
    unsigned conditionResult;
    unsigned conditionType;
    unsigned canProcess;

    unsigned jump; //?
};

//------------------------------------------------------------------------------------

void LoadRay(myType* rays, vec3* ray, unsigned rayNum)
{
#pragma HLS PIPELINE
#pragma HLS INLINE
	memcpy(ray, rays + rayNum * (3 * 2), sizeof(vec3) * 2); // sizeof(vec3)/sizeof(myType)
}

void LoadInstruction(InstructionExtracted* instructionList, InstructionExtracted* instructionOut, unsigned instructionNum)
{
#pragma HLS PIPELINE
	*instructionOut = instructionList[instructionNum];
}

void PrepareLocalRegisters(vec3* vecs,
						   InstructionExtracted* instructionIn,
						   unsigned char& it, unsigned short& writeRegister,
						   unsigned char& idx0, unsigned char& idx1, unsigned char& idxWr,
						   short int& matrixNum,
						   vec3* tmpVec0, vec3* tmpVec1)
{
//#pragma HLS PIPELINE
#pragma HLS INLINE

	it = instructionIn->it;
	writeRegister = instructionIn->wr;

	idxWr = instructionIn->idxWr;
	idx0 = instructionIn->idx0;
	idx1 = instructionIn->idx1;

	matrixNum = instructionIn->r0;

#define INST_GETV(reg) (reg < 0 ? \
							-vecs[-reg] : vecs[reg])

	*tmpVec0 = INST_GETV(instructionIn->r0);
	*tmpVec1 = INST_GETV(instructionIn->r1);

//	*tmpVec0 = vecs[instructionIn->r0];
//	*tmpVec1 = vecs[instructionIn->r1];

#undef INST_GETV
}

void Eval(InstructionFlow* flow,
		 unsigned char instructionType,
		 vec3* tmpVec0, vec3* tmpVec1,
		 unsigned char& idx0, unsigned char& idx1,
		 mat4* mats, short int matrixIdx,
		 vec3* result)
{
	/*static*/ bool condTemp;
#pragma HLS PIPELINE
//#pragma HLS INLINE

#define SET_BIT(mask, bitNum) (mask |= (1 << (bitNum)))
#define UNSET_BIT(mask, bitNum) (mask &= ~(1 << (bitNum)))
#define GET_BIT(mask, bitNum) (mask & (1 << bitNum))

	/*static*/ unsigned shift;

	myType sOp0 = tmpVec0->data[idx0];
	myType sOp1 = tmpVec1->data[idx1];
	myType minusFlag = 1.0;

	switch(instructionType)
	{
	case MOV:
		result->data[0] = sOp0;
		break;
	case MOVV:
		*result = *tmpVec0;
		break;
	case ADD:
		result->data[0] = sOp0 + sOp1;
		break;
	case ADDV:
		*result = *tmpVec0 + *tmpVec1;
		break;
/*	case MUL:
		result->data[0] = (ap_fixed<25, 13, AP_RND>)sOp0 * (ap_fixed<18, 9, AP_RND>)sOp1;
		break;*/
	case MUL:
		result->data[0] = (myTypeReduced)sOp0 * (myTypeReduced)sOp1;
		break;
	case MULV:
		*result = *tmpVec0 * (myTypeReduced)sOp1;
		break;
	case CMULV:
		*result = tmpVec0->CompWiseMul(*tmpVec1);
		break;
	case DOT:
		result->data[0] = *tmpVec0 * *tmpVec1;
		break;
	case DELTA:
		result->data[0] = (myTypeReduced)tmpVec0->data[1] * (myTypeReduced)tmpVec0->data[1] - (myTypeReduced)tmpVec0->data[0] * (myTypeReduced)tmpVec0->data[2];
		break;
#define GETM(reg) (mats[reg - TRANS_MAT_0])
	case MMULV:
		*result = GETM(matrixIdx).Transform(*tmpVec1);
		break;
	case MMULDIR:
		*result = GETM(matrixIdx).TransformDir(*tmpVec1);
		break;
	case TMMULDIR:
		*result = GETM(matrixIdx).TransposeTransformDir(*tmpVec1);
		break;
#undef GETM

	// -----SPECIAL INSTRUCTIONS

	case PREDIV: // !!! what if D < 0
		if (sOp1 < (myType)0.0)
		{
			sOp1 = -sOp1;
			minusFlag = -minusFlag;
		}
		shift = 32 - sOp1.countLeadingZeros(); //D >> bits
		result->data[0] = (shift >= 16) ? (sOp1 >> (shift - 16)) : (sOp1 << (16 - shift));		// Di
		result->data[1] = ((shift >= 16) ? (sOp0 >> (shift - 16)) : (sOp0 << (16 - shift))) * minusFlag;		// Ni
		result->data[2] = ((myType)48.0/(myType)17.0) - ((myTypeReduced)32.0/(myTypeReduced)17.0) * (myTypeReduced)result->data[0]; // x0
		break;

	case PRESQRT:
		shift = (32 - sOp0.countLeadingZeros()) >> 1;//(32 - __builtin_clz((unsigned)(tmpVec0->data[idx0]))) >> 1; //D >> bits
		result->data[0] = sOp0 >> 1;				// x/2
		result->data[1] = (shift >= 8) ? (((myType)1.0) >> (unsigned)(shift - 8)) : (((myType)1.0) << (unsigned)(8 - shift));			// y0
		result->data[2] = (myTypeReduced)result->data[0] * (myTypeReduced)result->data[1]; // x * y0
		break;

	// -----BRANCH CODE

	case IF_EQ:
	case IF_GE:
	case IF_GEQ:
		++flow->actualLvl;
		UNSET_BIT(flow->conditionType, flow->actualLvl);

		if (flow->actualLvl > 0 && (GET_BIT(flow->conditionResult, flow->actualLvl - 1) == GET_BIT(flow->conditionType, flow->actualLvl - 1)))
		{
			UNSET_BIT(flow->conditionResult, flow->actualLvl);
			UNSET_BIT(flow->canProcess, flow->actualLvl);
		}
		else
		{
			//#####################
			myType firstConditionalParam = sOp0;
			myType secondConditionalParam = sOp1;

			condTemp = ((instructionType == IF_EQ) ? (firstConditionalParam == secondConditionalParam):
							     										  ((instructionType == IF_GEQ) ? firstConditionalParam >= secondConditionalParam : (firstConditionalParam > secondConditionalParam))); // assign expression result to the current level
			//#####################
			if (condTemp == true) // get inside the IF block
			{
				SET_BIT(flow->conditionResult, flow->actualLvl);
				SET_BIT(flow->canProcess, flow->actualLvl);
			}
			else //if (conditionResult[actualLvl] == false)
			{
				// Look for ELSE/ENDIF instruction
				UNSET_BIT(flow->conditionResult, flow->actualLvl);
				UNSET_BIT(flow->canProcess, flow->actualLvl);

				// Optimization - skip instructions until ELSE or ENDIF, skip must be provided by the user

//				i += (unsigned)(inst.vec.data[1]);
			}
		}
		break;
		case ELSE:
			SET_BIT(flow->conditionType, flow->actualLvl);

			if (GET_BIT(flow->conditionResult, flow->actualLvl) == true || (flow->actualLvl > 0 && (GET_BIT(flow->conditionResult, flow->actualLvl - 1) == GET_BIT(flow->conditionType, flow->actualLvl - 1))))
			{
				SET_BIT(flow->conditionResult, flow->actualLvl);
				UNSET_BIT(flow->canProcess, flow->actualLvl);

				// Optimization - skip instructions until ENDIF because conditional result of associated IF was TRUE, skip must be provided by the user

//				i += (unsigned)(inst.vec.data[1]);
			}
			else if (GET_BIT(flow->conditionResult, flow->actualLvl) == false)
			{
				UNSET_BIT(flow->conditionResult, flow->actualLvl);
				SET_BIT(flow->canProcess, flow->actualLvl);
			}
			break;
		case ENDIF:	// closing brace of if...else block
			SET_BIT(flow->canProcess, flow->actualLvl);
			--flow->actualLvl;
			break;
	}
}

void UpdateRegister(InstructionFlow* flow,
					vec3* vecs, vec3& result,
					unsigned short wr, unsigned char idxWr)
{
#pragma HLS PIPELINE
//#pragma HLS INLINE
	if (flow->actualLvl < 0 || GET_BIT(flow->canProcess, flow->actualLvl))
	{
		if (idxWr < 3)
		{
			vecs[wr].data[idxWr] = result.data[0];
		}
		else vecs[wr] = result;
	}
}

void PutColor(pixelColorType* pixelColor, const vec3* colorRegister, unsigned rayNum)
{
//#pragma HLS PIPELINE
#pragma HLS INLINE
	static pixelColorType color[3];
#pragma HLS ARRAY_PARTITION variable=color complete dim=1
//	color[0] = (pixelColorType)((myType)255 * colorRegister->data[0]);
//	color[1] = (pixelColorType)((myType)255 * colorRegister->data[1]);
//	color[2] = (pixelColorType)((myType)255 * colorRegister->data[2]);

	color[0] = (pixelColorType)((myType)1 * colorRegister->data[0]);
	color[1] = (pixelColorType)((myType)1 * colorRegister->data[1]);
	color[2] = (pixelColorType)((myType)1 * colorRegister->data[2]);

	memcpy(pixelColor + rayNum * 3, color, sizeof(pixelColorType) * 3);
}

void ProcessInstructions(unsigned rayNum, unsigned rayDataSize, unsigned instructionNum, unsigned instructionListSize,
						InstructionExtracted* instructionList,
						InstructionFlow* flow,
						vec3* vecs,
						mat4* mats,
						material* materials,
						pixelColorType *pixelColor)
{
//#pragma HLS DEPENDENCE variable=vecs inter RAW true
//#pragma HLS DEPENDENCE variable=vecs intra WAR true

#pragma HLS PIPELINE
#pragma HLS INLINE

	/*static*/ unsigned char it, idxWr, idx0, idx1;
	/*static*/ unsigned short wr;

	/*static*/ short int matrixIdx;

	/*static */vec3 tmpVec0;
	/*static*/ vec3 tmpVec1;

	/*static*/ vec3 result;

	//########################################

	PrepareLocalRegisters(vecs,
			&instructionList[instructionNum], it, wr,
			idx0, idx1, idxWr,
			matrixIdx,
			&tmpVec0, &tmpVec1);

	Eval(flow,
		 it, &tmpVec0, &tmpVec1,
			 idx0, idx1,
		 mats, matrixIdx,
		 &result);

//	result.data[0] = tmpVec0 * tmpVec1;

	UpdateRegister(flow, vecs, result, wr, idxWr);
}

//------------------------------------------------------------------------------------


myType func(Instruction *instructionList, unsigned instructionListSize,
		    ExternalRegister *externalRegisterList, unsigned externalRegisterListSize,
			myType* rayData, unsigned rayDataSize,
							mat4 *transformationMatrices,
							material* sceneMaterials,
		//					unsigned sceneObjectsNum,
							light* sceneLights,
							unsigned sceneLightsNum,
							pixelColorType *pixelColor)
{
	myType out = 0;

#pragma HLS INTERFACE s_axilite port=sceneLightsNum bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=sceneLights bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=sceneLights offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=sceneMaterials bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=sceneMaterials offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=rayDataSize bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=rayData bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=rayData offset=slave bundle=MAXI_IN

//#pragma HLS INTERFACE s_axilite port=sceneObjectsNum bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=transformationMatrices bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=transformationMatrices offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=pixelColor bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=pixelColor offset=slave bundle=MAXI_OUT
//#pragma HLS DATA_PACK variable=outData field_level

#pragma HLS INTERFACE s_axilite port=externalRegisterListSize bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=externalRegisterList bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=externalRegisterList offset=slave bundle=MAXI_IN

#pragma HLS INTERFACE s_axilite port=instructionListSize bundle=AXI_LITE_1
#pragma HLS INTERFACE s_axilite port=instructionList bundle=AXI_LITE_1
#pragma HLS INTERFACE m_axi port=instructionList offset=slave bundle=MAXI_IN
//#pragma HLS DATA_PACK variable=instructionList

#pragma HLS INTERFACE s_axilite port=return bundle=AXI_LITE_1

//	myType scalars[LV_V3a_0];			 // additional for eps and distance along ray direction (t)
//	scalars[EPS] = RAY_HIT_EPS;

	/*static*/ vec3 vecs[TRANS_MAT_0];
//#pragma HLS ARRAY_PARTITION variable=vecs complete dim=1
//#pragma HLS RESOURCE variable=vecs core=RAM_T2P_BRAM
	static ExternalRegister externalRegisters[MAX_EXTERNAL_REGISTER_NUM];
	memcpy(externalRegisters, externalRegisterList, sizeof(ExternalRegister) * MAX_EXTERNAL_REGISTER_NUM);
	for (unsigned i = 0; i < MAX_EXTERNAL_REGISTER_NUM; ++i)
	{
#pragma HLS PIPELINE
		if (i < externalRegisterListSize)
		{
			vecs[externalRegisters[i].reg].data[0] = externalRegisters[i].x;
			vecs[externalRegisters[i].reg].data[1] = externalRegisters[i].y;
			vecs[externalRegisters[i].reg].data[2] = externalRegisters[i].z;
		}
	}
	vecs[LV_V3_ZERO_EPS_DIST].data[0] = 0.0;
	vecs[LV_V3_ZERO_EPS_DIST].data[1] = RAY_HIT_EPS;

	vecs[LV_V3_ONE_TWO_THREE].data[0] = 1.0;
	vecs[LV_V3_ONE_TWO_THREE].data[1] = 2.0;
	vecs[LV_V3_ONE_TWO_THREE].data[2] = 3.0;

	vecs[LV_V3_HALF_QUATER_THREEHALFS].data[0] = 0.5;
	vecs[LV_V3_HALF_QUATER_THREEHALFS].data[1] = 0.25;
	vecs[LV_V3_HALF_QUATER_THREEHALFS].data[2] = 1.5;

	vecs[LV_V3_NORMAL_100].data[0] = 1.0;
	vecs[LV_V3_NORMAL_100].data[1] = 0.0;
	vecs[LV_V3_NORMAL_100].data[2] = 0.0;

	vecs[LV_V3_NORMAL_010].data[0] = 0.0;
	vecs[LV_V3_NORMAL_010].data[1] = 1.0;
	vecs[LV_V3_NORMAL_010].data[2] = 0.0;

	vecs[LV_V3_NORMAL_001].data[0] = 0.0;
	vecs[LV_V3_NORMAL_001].data[1] = 0.0;
	vecs[LV_V3_NORMAL_001].data[2] = 1.0;
//#pragma HLS ARRAY_PARTITION variable=vecs complete dim=1
  // two additional values reserved for ray data + 2 for ShadeRec data

	mat4 mats[MAX_SCENE_OBJECTS * 2];
#pragma HLS ARRAY_PARTITION variable=mats complete dim=1
	memcpy(mats, transformationMatrices, sizeof(mat4) * MAX_SCENE_OBJECTS * 2);

	material materials[MAX_SCENE_OBJECTS];
//	memcpy(materials, sceneMaterials, sizeof(material) * MAX_SCENE_OBJECTS);

	light lights[MAX_SCENE_LIGHTS];
//	memcpy(lights, sceneLights, sizeof(light) * MAX_SCENE_LIGHTS);
//	RTSoftProcessor_GetLights:for (unsigned i = 0; i < MAX_SCENE_LIGHTS; ++i)
//	{
//#pragma HLS PIPELINE
//		if (i < sceneLightsNum)
//		{
//			vecs[LIGHT_RADIANCE012 + i / 3].data[i % 3] = lights[i].data[0];
//
//			vecs[LIGHT0_COLOR + i].data[0] = lights[i].data[1];
//			vecs[LIGHT0_COLOR + i].data[1] = lights[i].data[2];
//			vecs[LIGHT0_COLOR + i].data[2] = lights[i].data[3];
//
//			vecs[LIGHT0_POSITION + i].data[0] = lights[i].data[4];
//			vecs[LIGHT0_POSITION + i].data[1] = lights[i].data[5];
//			vecs[LIGHT0_POSITION + i - LV_V3a].data[2] = lights[i].data[6];
//
//			vecs[LIGHT0_K + i].data[0] = lights[i].data[7];
//			vecs[LIGHT0_K + i].data[1] = lights[i].data[8];
//			vecs[LIGHT0_K + i].data[2] = lights[i].data[9];
//		}
//	}

//	shadeRec hitResult;//, bestHit;

	vec3 registerValueOut;

	Instruction instINtemp[ASM_MAX_INSTRUCTION_COUNT];
	InstructionExtracted instructionsListExtracted[ASM_MAX_INSTRUCTION_COUNT];
//#pragma HLS ARRAY_PARTITION variable=instructionsListExtracted complete dim=1
	memcpy(instINtemp, instructionList, sizeof(Instruction) * ASM_MAX_INSTRUCTION_COUNT);
	for (unsigned i = 0; i < ASM_MAX_INSTRUCTION_COUNT; ++i)
	{
#pragma HLS PIPELINE
		if (i < instructionListSize) instructionsListExtracted[i] = instINtemp[i];
	}

	vec3 loadedRayData[2];

	unsigned visibleObjIdx;

	static InstructionFlow flow = {-1, 0, 0, 0, 0};
	unsigned short writeRegister;
	unsigned char idxWr;

//#pragma HLS DATAFLOW
	func_label2:for (unsigned rayNum = 0; rayNum < rayDataSize; ++rayNum)
	{
#pragma HLS LOOP_TRIPCOUNT min=1 max=1
#pragma HLS PIPELINE
//#pragma HLS DATAFLOW
		LoadRay(rayData, loadedRayData, rayNum);
		vecs[RAY_DIRECTION] = loadedRayData[0];
		vecs[RAY_ORIGIN] = loadedRayData[1];

		vecs[BESTHIT_COLOR].data[0] = vecs[BESTHIT_COLOR].data[1] = vecs[BESTHIT_COLOR].data[2] = 0.0;
		vecs[BESTHIT_ISHIT_DIST_OBJIDX].data[0] = (myType)0.0;
		vecs[BESTHIT_ISHIT_DIST_OBJIDX].data[1] = (myType)HUGE_REAL_VAL;

		flow.actualLvl = -1;
		SET_BIT(flow.canProcess, 0);

		func_label0:for (unsigned instructionNum = 0; instructionNum < instructionListSize; ++instructionNum)
		{
#pragma HLS LOOP_TRIPCOUNT min=10 max=10
//#pragma HLS DATAFLOW
#pragma HLS PIPELINE
			ProcessInstructions(rayNum, rayDataSize, instructionNum, instructionListSize,
					 instructionsListExtracted,
					 &flow,
					 vecs,
					 mats,
					 materials,
					 pixelColor);

//			if (tmpVec0.data[0] < (myType)0.0) ++instructionNum;
		}
		PutColor(pixelColor, &vecs[BESTHIT_COLOR], rayNum);
	}

	return out;
}
